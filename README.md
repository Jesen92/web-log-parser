# Ruby web log parser

Run script (with filepath)
```sh
$ ./bin/parser.rb webserver.log
```

Run all tests

```sh
$ rspec spec/tests
```

**Docker**

To run the script with docker use the existing 'parser' bash script.
On the first run the dockerfile will be built and 'bundle install' will be run.

Run parser
```sh
$ ./parser.sh run
```

Run RSpec Tests
```sh
$ ./parser.sh test
```

Run Rubocop
```sh
$ ./parser.sh rubocop
```

Stop and destroy all containers and images
```sh
$ ./parser.sh destroy
```