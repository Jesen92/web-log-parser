#!/bin/bash

command_list=("run" "test" "destroy" "rubocop")

if [ -z "$1" ] || [[ ! " ${command_list[@]} " =~ " $1 "  ]]
  then
    echo "Missing argument. Please enter one of the following: ['run', 'test', 'rubocop', 'destroy']"
    exit 1
fi

command="$1"

if [ ! "$(docker ps -q -f name=parser_ruby_1)" ]
  then
    # Create container and keep it running in the background
    docker-compose run -d --name parser_ruby_1 ruby bash
    # Bundle install
    docker exec -it parser_ruby_1 bash -c 'bundle install'
fi

if [ "${command}" = "run" ]
  then
    docker exec -it parser_ruby_1 bash -c 'bin/parser.rb webserver.log'
elif [ "${command}" = "test" ]
  then
    docker exec -it parser_ruby_1 bash -c 'bundle exec rspec spec/tests'
elif [ "${command}" = "rubocop" ]
  then
    docker exec -it parser_ruby_1 bash -c 'rubocop --disable-pending-cops'
elif [ "${command}" = "destroy" ]
  then
    docker stop $(docker ps -aq)
    docker rm $(docker ps -aq)
    docker rmi $(docker images -q)
    echo "Docker containers and images successfully destroyed"
fi
