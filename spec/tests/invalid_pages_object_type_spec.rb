# frozen_string_literal: true

require_relative '../../lib/custom_error/invalid_pages_object_type'
require 'rspec'

RSpec.describe CustomError::InvalidPagesObjectType do
  let(:invalid_pages_object_type) { CustomError::InvalidPagesObjectType.new }

  context 'Invalid pages object type' do
    it 'outputs default message when no message passed' do
      expect(invalid_pages_object_type.message).to eq('Invalid pages object type')
    end

    it 'returns given message when message passed' do
      expect(invalid_pages_object_type.message(default: 'Custom message')).to eq('Custom message')
    end
  end
end
