# frozen_string_literal: true

require_relative '../../lib/validator/base_validator'
require_relative '../../lib/custom_error/validator_object_not_implemented'
require 'rspec'

RSpec.describe Validator::BaseValidator do
  let(:base_validator) { Validator::BaseValidator.new }

  context 'base validator' do
    it 'should raise CustomError::ValidatorObjectNotImplemented when log_valid? is not implemented' do
      expect { base_validator.log_valid?(path: '', ip_address: '', index: 0) }
        .to raise_error(CustomError::ValidatorObjectNotImplemented)
    end
  end
end
