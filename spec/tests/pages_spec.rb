# frozen_string_literal: true

require_relative '../../lib/page_visit_tracker/pages'
require 'rspec'

RSpec.describe PageVisitTracker::Pages do
  let(:pages) { PageVisitTracker::Pages.new }

  context 'add page visits' do
    it 'should add new page' do
      expect(pages.all.length).to eq(0)

      pages.add_page_visit(
        {
          path: '/index',
          ip: '172.0.12.18'
        }
      )

      expect(pages.all.length).to eq(1)
    end

    it 'should add a different page visit' do
      pages.add_page_visit(
        {
          path: '/index',
          ip: '172.0.12.18'
        }
      )

      expect(pages.all.length).to eq(1)

      pages.add_page_visit(
        {
          path: '/home',
          ip: '172.0.12.18'
        }
      )

      expect(pages.all.length).to eq(2)
    end

    it 'should add same page visits' do
      pages.add_page_visit(
        {
          path: '/index',
          ip: '172.0.12.18'
        }
      )

      expect(pages.all.length).to eq(1)
      expect(pages.all.first.visits).to eq(1)

      pages.add_page_visit(
        {
          path: '/index',
          ip: '172.0.12.18'
        }
      )

      expect(pages.all.length).to eq(1)
      expect(pages.all.first.visits).to eq(2)
    end
  end
end
