# frozen_string_literal: true

require_relative '../../lib/pages_log_parser'
require_relative '../../lib/string'
require_relative '../../lib/custom_error/invalid_file_object'
require_relative '../../lib/custom_error/invalid_pages_object_type'
require_relative '../../lib/custom_error/invalid_validator_object'

require 'rspec'

RSpec.describe PagesLogParser do
  let(:file) { File.open('spec/fixtures/webserver.log', 'r') }
  let(:file_missing_ip) { File.open('spec/fixtures/webserver_ip_missing.log', 'r') }
  let(:file_missing_route) { File.open('spec/fixtures/webserver_path_missing.log', 'r') }
  let(:log_validator) { Validator::LogValidator.new }
  let(:pages) { PageVisitTracker::Pages.new }

  context 'parsing file' do
    it 'should not output errors on valid file' do
      expect { PagesLogParser.new({ file: file }).call }.to output(/0 errors found/).to_stdout
      expect { PagesLogParser.new({ file: file }).call }.to_not output(/Invalid path at lines:/).to_stdout
      expect { PagesLogParser.new({ file: file }).call }.to_not output(/Missing IP address at lines:/).to_stdout
    end

    it 'should output invalid path when file has invalid path' do
      expect { PagesLogParser.new({ file: file_missing_route }).call }.to output(/Invalid path at lines:/).to_stdout
    end

    it 'should output missing ip address lines when file has missing ip addresses' do
      expect { PagesLogParser.new({ file: file_missing_ip }).call }.to output(/Missing IP address at lines:/).to_stdout
    end

    it 'should print an error message when file is missing' do
      expect { PagesLogParser.new({ file: 'asd' }).call }.to output(/Invalid file object/).to_stdout
    end
  end
end
