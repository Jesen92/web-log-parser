# frozen_string_literal: true

require_relative '../../lib/printer/page_printer'
require_relative '../../lib/page_visit_tracker/pages'
require_relative '../../lib/parser/log_parser'
require_relative '../../lib/validator/log_validator'

require 'rspec'
require 'byebug'

RSpec.describe Printer::PagePrinter do
  before(:all) {
    @pages = PageVisitTracker::Pages.new
    file = File.open('spec/fixtures/webserver.log', 'r')
    log_validator = Validator::LogValidator.new

    Parser::LogParser.parse(file_parse_from: file, pages_parse_to: @pages, custom_log_validator: log_validator)
  }

  context 'page printer' do
    it 'should iterate sent block sorted by visitors' do
      printed_values = []

      Printer::PagePrinter.each_page_sorted_by :visitors, @pages.all do |page|
        printed_values << page.visits
      end

      expect(printed_values.each_cons(2).all? { |left, right| right <= left }).to be true
    end

    it 'should iterate sent block sorted by unique visitors' do
      printed_values = []

      Printer::PagePrinter.each_page_sorted_by :unique_visitors, @pages.all do |page|
        printed_values << page.unique_visitors_ip.length
      end

      expect(printed_values.each_cons(2).all? { |left, right| right <= left }).to be true
    end
  end
end
