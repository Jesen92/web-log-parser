# frozen_string_literal: true

require_relative '../../lib/parser/log_parser'
require_relative '../../lib/validator/log_validator'
require_relative '../../lib/page_visit_tracker/pages'
require_relative '../../lib/custom_error/invalid_file_object'
require_relative '../../lib/custom_error/invalid_pages_object_type'
require_relative '../../lib/custom_error/invalid_validator_object'
require 'rspec'

RSpec.describe Parser::LogParser do
  let(:file) { File.open('spec/fixtures/webserver.log', 'r') }
  let(:log_validator) { Validator::LogValidator.new }
  let(:pages) { PageVisitTracker::Pages.new }

  context 'parsing from file to pages' do
    it 'should parse logs when given valid parameters' do
      Parser::LogParser.parse(file_parse_from: file, pages_parse_to: pages, custom_log_validator: log_validator)

      expect(pages.all).to_not be_empty
    end

    it 'raises error when invalid File object' do
      expect {
        Parser::LogParser.parse(file_parse_from: 'asd', pages_parse_to: pages, custom_log_validator: log_validator)
      }.to raise_error(CustomError::InvalidFileObject)
    end

    it 'raises error when invalid Pages object' do
      expect { Parser::LogParser.parse(file_parse_from: file, pages_parse_to: [], custom_log_validator: log_validator) }
        .to raise_error(CustomError::InvalidPagesObjectType)
    end

    it 'raises error when invalid File object' do
      expect { Parser::LogParser.parse(file_parse_from: file, pages_parse_to: pages, custom_log_validator: []) }
        .to raise_error(CustomError::InvalidValidatorObject)
    end
  end
end
