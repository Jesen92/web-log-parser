# frozen_string_literal: true

require_relative '../../lib/validator/log_validator'
require 'rspec'

RSpec.describe Validator::LogValidator do
  let(:validator) { Validator::LogValidator.new }

  context 'outcome should be valid' do
    it 'should be valid when given path and ip address' do
      expect(validator.log_valid?(path: '/index', ip_address: '172.0.17.18', index: 0))
        .to be true
    end
  end

  context 'outcome should be invalid' do
    it 'should be invalid when invalid path' do
      expect(validator.log_valid?(path: 'index', ip_address: '172.0.17.18', index: 0))
        .to be false
    end

    it 'should be invalid when missing ip' do
      expect(validator.log_valid?(path: 'index', ip_address: nil, index: 0))
        .to be false
    end
  end
end
