# frozen_string_literal: true

require_relative '../../lib/custom_error/invalid_validator_object'
require 'rspec'

RSpec.describe CustomError::InvalidValidatorObject do
  let(:invalid_validator_object) { CustomError::InvalidValidatorObject.new }

  context 'Invalid pages object type' do
    it 'outputs default message when no message passed' do
      expect(invalid_validator_object.message).to eq('Object must be type Validator::BaseValidator')
    end

    it 'returns given message when message passed' do
      expect(invalid_validator_object.message(default: 'Custom message')).to eq('Custom message')
    end
  end
end
