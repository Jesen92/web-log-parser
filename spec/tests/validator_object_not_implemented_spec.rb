# frozen_string_literal: true

require_relative '../../lib/custom_error/validator_object_not_implemented'
require 'rspec'

RSpec.describe CustomError::ValidatorObjectNotImplemented do
  let(:validator_object_not_implemented) { CustomError::ValidatorObjectNotImplemented.new }

  context 'Invalid pages object type' do
    it 'outputs default message when no message passed' do
      expect(validator_object_not_implemented.message).to eq('log_valid? Must be overridden')
    end

    it 'returns given message when message passed' do
      expect(validator_object_not_implemented.message(default: 'Custom message')).to eq('Custom message')
    end
  end
end
