# frozen_string_literal: true

require_relative 'page_visit_tracker/pages'
require_relative 'validator/log_validator'
require_relative 'printer/printer'
require_relative 'printer/error_printer'
require_relative 'parser/log_parser'
require_relative 'custom_error/base_custom_error'

class PagesLogParser
  include PageVisitTracker
  include Validator
  include Printer
  include Parser
  include CustomError

  def initialize params
    @file = params[:file]
    @pages_store = params[:pages] || Pages.new
    @log_validator = params[:log_validator] || LogValidator.new
  end

  def call
    parse_log_file
    print_formated_log_file
    print_error_list
  rescue BaseCustomError => e
    puts e.message.red
  end

  private

  attr_accessor :pages_store, :log_validator
  attr_reader :file

  def parse_log_file
    puts 'Parsing file...'.light_blue
    LogParser.parse(file_parse_from: file, pages_parse_to: pages_store, custom_log_validator: log_validator)
    puts 'File parsed'.green

    pages_store
  end

  def print_formated_log_file
    # Print by visitors
    Printer.print_pages_sorted_by_visitors pages_store.all

    # Print by unique visitors
    Printer.print_pages_sorted_by_unique_visitors pages_store.all
  end

  def print_error_list
    Printer.print_error_list(log_validator.parse_error_list)
  end
end
