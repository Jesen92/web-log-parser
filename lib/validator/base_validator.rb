# frozen_string_literal: true

module Validator
  require_relative '../custom_error/validator_object_not_implemented'

  class BaseValidator
    def log_valid?(path:, ip_address:, index:) # rubocop:disable Lint/UnusedMethodArgument
      raise CustomError::ValidatorObjectNotImplemented
    end
  end
end
