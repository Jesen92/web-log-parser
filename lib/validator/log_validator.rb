# frozen_string_literal: true

module Validator
  # Not used - but should be used for validating ip addresses
  require 'ipaddr'
  require_relative 'base_validator'

  class LogValidator < BaseValidator
    attr_reader :parse_error_list

    def initialize
      @parse_error_list = {
        invalid_paths: [],
        invalid_ip_addresses: []
      }
    end

    def log_valid?(path:, ip_address:, index:)
      path_valid?(path, index) &&
        ip_address_valid?(ip_address, index)
    end

    private

    def path_valid? path, index
      if %r{^[^\/]+\/[^\/].*$|^\/[^\/].*$}.match(path).nil?
        @parse_error_list[:invalid_paths] << index

        return false
      end

      true
    end

    def ip_address_valid? ip_address, index
      if ip_address.nil? # && (IPAddr.new(ip_address) rescue nil).nil?
        @parse_error_list[:invalid_ip_addresses] << index

        return false
      end

      true
    end
  end
end
