# frozen_string_literal: true

module PageVisitTracker
  class Page
    attr_reader :path, :visits, :unique_visitors_ip

    def initialize params
      @path = params[:path]
      @visits = 0
      @unique_visitors_ip = []
    end

    def add_new_visit(visitor_ip:)
      @visits += 1
      @unique_visitors_ip << visitor_ip unless @unique_visitors_ip.include? visitor_ip
    end
  end
end
