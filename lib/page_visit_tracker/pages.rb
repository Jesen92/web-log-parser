# frozen_string_literal: true

module PageVisitTracker
  require_relative 'page'

  class Pages
    include Enumerable

    def initialize
      @pages = []
    end

    def add_page_visit params
      page = find_by_path params[:path]

      page = add_new_page params[:path] if page.nil?
      page.add_new_visit(visitor_ip: params[:ip_address])
    end

    def all
      @pages
    end

    private

    attr_reader :pages

    def find_by_path path
      pages.find { |page| page.path === path }
    end

    def add_new_page path
      page = Page.new(
        {
          path: path
        }
      )

      pages << page

      page
    end
  end
end
