# frozen_string_literal: true

module CustomError
  require_relative 'base_custom_error'

  class InvalidFileObject < BaseCustomError
    def message(default: 'Invalid file object')
      default
    end
  end
end
