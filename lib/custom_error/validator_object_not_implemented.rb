# frozen_string_literal: true

module CustomError
  require_relative 'base_custom_error'

  class ValidatorObjectNotImplemented < BaseCustomError
    def message(default: 'log_valid? Must be overridden')
      default
    end
  end
end
