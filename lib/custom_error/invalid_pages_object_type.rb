# frozen_string_literal: true

module CustomError
  require_relative 'base_custom_error'

  class InvalidPagesObjectType < BaseCustomError
    def message(default: 'Invalid pages object type')
      default
    end
  end
end
