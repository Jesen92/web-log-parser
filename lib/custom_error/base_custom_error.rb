# frozen_string_literal: true

module CustomError
  class BaseCustomError < StandardError
  end
end
