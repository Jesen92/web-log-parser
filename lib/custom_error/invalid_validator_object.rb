# frozen_string_literal: true

module CustomError
  require_relative 'base_custom_error'

  class InvalidValidatorObject < BaseCustomError
    def message(default: 'Object must be type Validator::BaseValidator')
      default
    end
  end
end
