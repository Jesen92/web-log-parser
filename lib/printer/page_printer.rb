# frozen_string_literal: true

module Printer
  class PagePrinter
    class << self
      SORT_BY_VISITORS = :visitors
      SORT_BY_UNIQUE_VISITORS = :unique_visitors

      def each_page_sorted_by sort_by, pages, &block
        case sort_by
        when SORT_BY_VISITORS
          pages = sort_by_visits pages
        when SORT_BY_UNIQUE_VISITORS
          pages = sort_by_unique_visits pages
        end

        pages.each(&block)
      end

      private

      def sort_by_visits pages
        pages.sort_by { |page| -page.visits }
      end

      def sort_by_unique_visits pages
        pages.sort_by { |page| -page.unique_visitors_ip.length }
      end
    end
  end
end
