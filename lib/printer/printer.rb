# frozen_string_literal: true

module Printer
  require_relative 'page_printer'
  require_relative 'error_printer'

  class Printer
    def self.print_pages_sorted_by_visitors pages
      output = "\n"
      output += "|#{'Page path'.ljust(20).blue}|#{'No. of visits'.ljust(20).blue}|\n"
      output += "-------------------------------------------\n"
      PagePrinter.each_page_sorted_by :visitors, pages do |page|
        output += "|#{page.path.ljust(20).green}|#{page.visits.to_s.ljust(20).green}|\n"
      end

      puts output + "\n"
    end

    def self.print_pages_sorted_by_unique_visitors pages
      output = "\n"
      output += "|#{'Page path'.ljust(20).blue}|#{'Unique visitors'.ljust(20).blue}|\n"
      output += "-------------------------------------------\n"
      PagePrinter.each_page_sorted_by :unique_visitors, pages do |page|
        output += "|#{page.path.ljust(20).green}|#{page.unique_visitors_ip.length.to_s.ljust(20).green}|\n"
      end

      puts output + "\n"
    end

    def self.print_error_list error_list
      puts ErrorPrinter.print_error_list error_list
    end
  end
end
