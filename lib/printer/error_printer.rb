# frozen_string_literal: true

module Printer
  class ErrorPrinter
    class << self
      def print_error_list error_list
        prettify_error_list error_list
      end

      private

      def prettify_error_list error_list
        error_messages = ''

        error_messages += invalid_path error_list
        error_messages += invalid_ip_addresses error_list

        return '0 errors found'.green if error_messages.empty?

        error_messages
      end

      def invalid_path error_list
        invalid_path_message = ''

        if error_list[:invalid_paths].any?
          invalid_path_message += 'Invalid path at lines: '.red
          invalid_path_message += error_list[:invalid_paths].join(', ')
          invalid_path_message += "\n"
        end

        invalid_path_message
      end

      def invalid_ip_addresses error_list
        invalid_ip_addresses_messages = ''

        if error_list[:invalid_ip_addresses].any?
          invalid_ip_addresses_messages += 'Missing IP address at lines: '.red
          invalid_ip_addresses_messages += error_list[:invalid_ip_addresses].join(', ')
          invalid_ip_addresses_messages += "\n"
        end

        invalid_ip_addresses_messages
      end
    end
  end
end
