# frozen_string_literal: true

module Parser
  require_relative '../page_visit_tracker/pages'
  require_relative '../validator/base_validator'
  require_relative '../custom_error/invalid_file_object'
  require_relative '../custom_error/invalid_pages_object_type'
  require_relative '../custom_error/invalid_validator_object'

  class LogParser
    class << self
      def parse(file_parse_from:, pages_parse_to:, custom_log_validator:)
        raise CustomError::InvalidFileObject unless File.file?(file_parse_from)
        raise CustomError::InvalidPagesObjectType unless pages_parse_to.instance_of? PageVisitTracker::Pages
        raise CustomError::InvalidValidatorObject unless custom_log_validator.is_a? Validator::BaseValidator

        file_parse(file_parse_from, pages_parse_to, custom_log_validator)
      end

      private

      def file_parse(
        file_parse_from,
        pages_parse_to,
        custom_log_validator
      )
        file_parse_from.each_line.with_index do |line, index|
          path, ip_address = line.split(' ')

          next if line.split(' ').empty? ||
                  !custom_log_validator.log_valid?(path: path, ip_address: ip_address, index: index + 1)

          pages_parse_to.add_page_visit({ path: path.strip, ip_address: ip_address.strip })
        end
      end
    end
  end
end
