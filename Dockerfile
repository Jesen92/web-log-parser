FROM ruby:2.7.1

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

RUN mkdir /parser
WORKDIR /parser
COPY Gemfile /parser/Gemfile
COPY Gemfile.lock /parser/Gemfile.lock

RUN bundle install

COPY . /parser