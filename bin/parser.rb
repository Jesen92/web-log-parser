#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/string'
require_relative '../lib/pages_log_parser'

abort 'Filename required'.red if ARGV.empty?
abort 'File extension should be *.log'.red if ARGV[0].slice(-4, 4) != '.log'
abort 'Missing log file'.red unless File.exist? ARGV[0].to_s
abort 'Log file is empty'.red if File.file?(ARGV[0].to_s) && File.zero?(ARGV[0].to_s)

file = File.open(ARGV[0], 'r')

PagesLogParser.new({ file: file }).call
